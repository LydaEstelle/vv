import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homevv',
  templateUrl: './homevv.component.html',
  styleUrls: ['./homevv.component.scss']
})
export class HomevvComponent implements OnInit {

  url : String | undefined

  constructor() { }

  ngOnInit(): void {

    this.url = "http://localhost:4200/"

    console.log(this.url)

  }



}
