import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomevvComponent } from './homevv.component';

describe('HomevvComponent', () => {
  let component: HomevvComponent;
  let fixture: ComponentFixture<HomevvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomevvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomevvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
