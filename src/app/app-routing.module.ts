import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomevvComponent } from './homevv/homevv.component';

const routes: Routes = [
  { path:'',component:HomevvComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
